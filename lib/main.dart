import 'package:flutter/material.dart';
import 'package:flutter_sequence_animation/flutter_sequence_animation.dart';

void main() => runApp(MaterialApp(
      home: AnimationTest(),
    ));

class AnimationTest extends StatefulWidget {
  @override
  _AnimationState createState() => _AnimationState();
}

class _AnimationState extends State<AnimationTest>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  SequenceAnimation sequenceAnimation;

  @override
  void initState() {
    super.initState();
    //initialize controller with vsync, no need for duration, using sequenceAnimation
    controller = AnimationController(vsync: this);
    //initialize sequenceanimations
    //note: sequences have to be in order, and cannot overlap the same properties
    sequenceAnimation = SequenceAnimationBuilder()
        .addAnimatable(
          //Animatable is a single animation object
          //with color changing tween and duration
          animatable: ColorTween(begin: Colors.red, end: Colors.green),
          from: Duration(seconds: 0),
          to: Duration(seconds: 2),
          tag: "color",
        )
        //second animatable, to change color from green to blue
        .addAnimatable(
          //Animatable is a single animation object
          //with color changing tween and duration
          animatable: ColorTween(begin: Colors.green, end: Colors.blue),
          from:
              Duration(seconds: 2), //make sure seconds don't overlap with first
          to: Duration(seconds: 4),
          tag: "color",
        )
        .addAnimatable(
          //Animatable go back to red
          animatable: ColorTween(begin: Colors.blue, end: Colors.red),
          from: Duration(seconds: 4), //make sure seconds don't overlap with
          to: Duration(seconds: 6), //same tag and durations
          tag: "color",
        )
        //animatable for changing size, use Tween<double>
        //duration can overlap, different animatable, change tag
        .addAnimatable(
          animatable: Tween<double>(begin: 200, end: 500),
          from: Duration(seconds: 0), //duration overlaps, but different tags
          to: Duration(seconds: 4),
          tag: "size1",
        )
        .addAnimatable(
          animatable: Tween<double>(begin: 10, end: 600),
          from: Duration(seconds: 0), //duration overlaps, but different tags
          to: Duration(seconds: 4),
          tag: "size2",
        )
        .addAnimatable(
          animatable: Tween<double>(begin: 500, end: 100), //connect sizes
          from: Duration(seconds: 4),
          to: Duration(seconds: 6),
          tag: "size1",
        )
        .addAnimatable(
          animatable: Tween<double>(begin: 600, end: 100),
          from: Duration(seconds: 4),
          to: Duration(seconds: 6),
          tag: "size2",
        )
        //animate border radius tween
        .addAnimatable(
            animatable: BorderRadiusTween(
              begin: BorderRadius.circular(0),
              end: BorderRadius.circular(100),
            ),
            from: Duration(seconds: 1),
            to: Duration(seconds: 2),
            tag: "radius")
        .animate(controller
          ..addStatusListener((status) {
            if (status == AnimationStatus.completed) {
              controller.reverse();
            } else if (status == AnimationStatus.dismissed) {
              controller.forward();
            }
          }));
    //..addStatusListener((state) => print('$state'));
    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Animation test'),
      ),
      body: Container(
        padding: EdgeInsets.all(32.0),
        child: Center(
          child: AnimatedBuilder(
            //controller is the animation
            animation: controller,
            builder: (BuildContext context, Widget child) {
              return Container(
                  //return container
                  //add animations to dimensions/features
                  height: sequenceAnimation["size1"].value,
                  width: sequenceAnimation["size2"].value,
                  decoration: BoxDecoration(
                    //add border to container
                    borderRadius: sequenceAnimation["radius"].value,
                    color: sequenceAnimation["color"].value,
                  ));
            },
          ),
        ),
      ),
    );
  }

  //destructor
  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
